#
# Copyright (C) 2020 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Permissions
PRODUCT_COPY_FILES += \
    vendor/xiaomi/mojito-miuicamera/configs/default-permissions/miuicamera-permissions.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/default-permissions/miuicamera-permissions.xml \
    vendor/xiaomi/mojito-miuicamera/configs/permissions/privapp-permissions-miui.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-miui.xml \
    vendor/xiaomi/mojito-miuicamera/configs/permissions/privapp-permissions-miuiextraphoto.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-miuiextraphoto.xml

PRODUCT_COPY_FILES += \
    vendor/xiaomi/mojito-miuicamera/configs/device_features/mojito.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/device_features/mojito.xml \
    vendor/xiaomi/mojito-miuicamera/configs/device_features/sunny.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/device_features/sunny.xml

# Sysconfig
PRODUCT_COPY_FILES += \
    vendor/xiaomi/mojito-miuicamera/configs/sysconfig/miuicamera-hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/miuicamera-hiddenapi-package-whitelist.xml

# Props
PRODUCT_PRODUCT_PROPERTIES += \
	ro.com.google.lens.oem_camera_package=com.android.camera,org.lineageos.aperture

PRODUCT_SYSTEM_PROPERTIES += \
    persist.vendor.camera.privapp.list=com.android.camera,org.lineageos.aperture \
    vendor.camera.aux.packagelist=com.android.camera,org.lineageos.aperture,org.pixelexperience.faceunlock \
    ro.hardware.camera=xiaomi \
	ro.miui.notch=1


$(call inherit-product, vendor/xiaomi/mojito-miuicamera/common/common-vendor.mk)
